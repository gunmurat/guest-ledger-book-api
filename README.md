# GuestLedgerBookAPI

Guest Ledger Book API that is developed with Go Fiber and MongoDB

## Getting started

### Test
```
go test ./test
```

### Run
```
go run main.go
```
