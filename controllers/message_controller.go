package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/guest-ledger-book/services"
)

type MessageController interface {
	CreateMessage(ctx *fiber.Ctx) error
	GetAllMessages(ctx *fiber.Ctx) error
}

type MessageControllerImpl struct {
	messageService services.MessageService
}

func NewMessageController(messageService services.MessageService) MessageController {
	return &MessageControllerImpl{
		messageService: messageService,
	}
}

func (m *MessageControllerImpl) CreateMessage(ctx *fiber.Ctx) error {
	msg, err := m.messageService.CreateMessage(ctx.Body())
	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"error":   err.Error(),
			"success": false,
		})
	}
	return ctx.Status(201).JSON(fiber.Map{
		"data":    msg,
		"success": true,
	})
}

func (m *MessageControllerImpl) GetAllMessages(ctx *fiber.Ctx) error {
	msgs, err := m.messageService.GetAllMessages()
	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"error":   err.Error(),
			"success": false,
		})
	}
	return ctx.Status(200).JSON(fiber.Map{
		"data":    msgs,
		"success": true,
	})
}
