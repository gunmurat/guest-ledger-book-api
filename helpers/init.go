package helpers

import (
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/gunmurat/guest-ledger-book/config"
)

func Initialize() {
	err := godotenv.Load("/Users/muratgun/Dev/GoProjects/GuestLedgerBookAPI/.env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	err = config.ConnectMongo()
	if err != nil {
		log.Fatal("Error connecting to mongo")
	}
}
