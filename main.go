package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"gitlab.com/gunmurat/guest-ledger-book/helpers"
	"gitlab.com/gunmurat/guest-ledger-book/routes"
)

func init() {
	helpers.Initialize()
}

func main() {
	app := fiber.New()

	app.Use(logger.New())

	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowMethods: "GET, POST, PUT, DELETE, OPTIONS",
		AllowHeaders: "Content-Type, Authorization, Content-Length, X-Requested-With",
	}))

	routes.MessageRoutes(app)

	app.Listen(":5000")
}
