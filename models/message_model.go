package models

import (
	"reflect"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Message struct {
	ID      primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Email   string             `json:"email" bson:"email"`
	Message string             `json:"message" bson:"message"`
}

func GetStringFields(st interface{}) []string {
	var fields []string
	t := reflect.TypeOf(st)

	for i := 0; i < t.NumField(); i++ {
		if t.Field(i).Type.Kind() == reflect.String {
			fields = append(fields, t.Field(i).Tag.Get("bson"))
		}
	}

	return fields
}
