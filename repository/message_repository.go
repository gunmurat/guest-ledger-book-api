package repository

import (
	"context"

	"gitlab.com/gunmurat/guest-ledger-book/config"
	"gitlab.com/gunmurat/guest-ledger-book/helpers"
	"gitlab.com/gunmurat/guest-ledger-book/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type MessageRepository interface {
	Create(message models.Message) (*models.Message, error)
	GetAllMessages() ([]models.Message, error)
}

type MessageRepositoryImpl struct {
	collection string
}

func NewMessageRepository(collection string) MessageRepository {
	return &MessageRepositoryImpl{
		collection: collection,
	}
}

func (m *MessageRepositoryImpl) Create(message models.Message) (*models.Message, error) {

	res, err := config.GetCollection(m.collection).InsertOne(context.TODO(), message)
	if err != nil {
		return nil, err
	}

	message.ID = res.InsertedID.(primitive.ObjectID)

	return &message, nil
}

func (m *MessageRepositoryImpl) GetAllMessages() ([]models.Message, error) {

	cursor, err := config.GetCollection(m.collection).Find(context.TODO(), helpers.SFilter, helpers.Opt)
	if err != nil {
		return nil, err
	}

	var messages []models.Message

	for cursor.Next(context.TODO()) {
		var message models.Message
		cursor.Decode(&message)
		messages = append(messages, message)
	}

	return messages, nil
}
