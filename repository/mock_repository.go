package repository

import (
	"gitlab.com/gunmurat/guest-ledger-book/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type MockRepositoryImpl struct {
}

func NewMockRepository() MessageRepository {
	return &MockRepositoryImpl{}
}

func (m *MockRepositoryImpl) Create(message models.Message) (*models.Message, error) {
	return &message, nil
}

func (m *MockRepositoryImpl) GetAllMessages() ([]models.Message, error) {
	return []models.Message{
		{
			ID:      primitive.NewObjectID(),
			Email:   "testmail@gmail.com",
			Message: "Test Message 1",
		},
		{
			ID:      primitive.NewObjectID(),
			Email:   "testmail1@gmail.com",
			Message: "Test Message 2",
		},
	}, nil
}
