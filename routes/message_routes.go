package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/guest-ledger-book/controllers"
	"gitlab.com/gunmurat/guest-ledger-book/repository"
	"gitlab.com/gunmurat/guest-ledger-book/services"
)

var controller = controllers.NewMessageController(services.NewMessageService(repository.NewMessageRepository("messages")))

func MessageRoutes(app *fiber.App) {
	app.Post("/messages", controller.CreateMessage)
	app.Get("/messages", controller.GetAllMessages)
}
