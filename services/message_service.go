package services

import (
	"encoding/json"
	"errors"

	"gitlab.com/gunmurat/guest-ledger-book/models"
	"gitlab.com/gunmurat/guest-ledger-book/repository"
)

type MessageService interface {
	CreateMessage(message []byte) (*models.Message, error)
	GetAllMessages() ([]models.Message, error)
}

type MessageServiceImpl struct {
	MessageRepository repository.MessageRepository
}

func NewMessageService(messageRepository repository.MessageRepository) MessageService {
	return &MessageServiceImpl{
		MessageRepository: messageRepository,
	}
}

func (m *MessageServiceImpl) CreateMessage(message []byte) (*models.Message, error) {
	var model models.Message

	err := json.Unmarshal(message, &model)
	if err != nil {
		return nil, errors.New("message can not decode")
	}
	return m.MessageRepository.Create(model)
}

func (m *MessageServiceImpl) GetAllMessages() ([]models.Message, error) {
	return m.MessageRepository.GetAllMessages()
}
