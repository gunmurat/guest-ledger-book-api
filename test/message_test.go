package test

import (
	"testing"

	"gitlab.com/gunmurat/guest-ledger-book/repository"
	"gitlab.com/gunmurat/guest-ledger-book/services"
	"gotest.tools/assert"
)

var service = services.NewMessageService(repository.NewMockRepository())

func TestCreateMessage(t *testing.T) {
	message := []byte(`{
		"email": "testmail@gmail.com",
		"message": "Test Message 1"
	}`)
	m, err := service.CreateMessage(message)

	assert.NilError(t, err)
	assert.Equal(t, m.Email, "testmail@gmail.com")
}

func TestGetAllMessages(t *testing.T) {
	ms, err := service.GetAllMessages()

	assert.NilError(t, err)
	assert.Equal(t, len(ms), 2)
}
